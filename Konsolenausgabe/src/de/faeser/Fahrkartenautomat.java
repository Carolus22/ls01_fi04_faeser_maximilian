package de.faeser;

import java.util.Scanner;

public class Fahrkartenautomat {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);

		double zuZahlenderBetrag;
		double eingezahlterGesamtbetrag;
		String antwort = "ja";

		do {

		// Fahrkartenauswahl
		// -------------------
		zuZahlenderBetrag = fahrkartenAuswahl(tastatur);
		
		// Geldeinwurf
		// -----------
		eingezahlterGesamtbetrag = bezahlen(zuZahlenderBetrag, tastatur);

		// Fahrscheinausgabe
		// -----------------
		ausgabe();

		// R�ckgeldberechnung und -Ausgabe
		// -------------------------------
		r�ckgabe(eingezahlterGesamtbetrag, zuZahlenderBetrag);
		
		System.out.println("Wollen Sie weitere Fahrkarten erwerben?");
		antwort = tastatur.next();
		
		} while (antwort.equals("ja"));
		
	}

	public static double runden(double aktWert) {

		return Math.round(aktWert * 100) / 100d;
	}

	public static double fahrkartenAuswahl(Scanner tastatur) {

		double zahlBetrag = 0;
//		alte Definition von Preis & Anzahl
//		double preisa = 1.60;
//		double preisb = 2.50;
//		double preisc = 3.00;
//		int anzahla = 0;
//		int anzahlb = 0;
//		int anzahlc = 0;
		int auswahl = 0;
//		im Code anpassbarer Array f�r den Preis
		double [] preis = {1.60, 2.50, 3.00};
		int [] anzahl = {0, 0, 0};
		
		String dev = "0";
		System.out.println("Anpassungsmodus der Fahrkartenpreise starten?");
		dev = tastatur.next();
		while (dev.equals("7734")) {
			System.out.println("Preis f�r Fahrkartentyp A anpassen:");
			preis[0] = tastatur.nextDouble();
			System.out.println("Preis f�r Fahrkartentyp B anpassen:");
			preis[1] = tastatur.nextDouble();
			System.out.println("Preis f�r Fahrkartentyp C anpassen:");
			preis[2] = tastatur.nextDouble();
			dev = "0";
			System.out.println("Der Anpassungsmodus wurde beendet.");
			}
		
		
		while (auswahl != 4) {
		System.out.println("Die folgenden Fahrkartentypen stehen zur Verf�gung:");
		System.out.printf("Fahrkartentyp A - %.2f Euro. \n", (preis[0]));
		System.out.printf("Fahrkartentyp B - %.2f Euro. \n", (preis[1]));
		System.out.printf("Fahrkartentyp C - %.2f Euro. \n", (preis[2]));
		System.out.println("==========");
		System.out.println("Gew�hlte Fahrkarten: ");
		if (anzahl[0] > 0) {
		System.out.println(anzahl[0] + " * Typ A");
		}
		if (anzahl[1] > 0) {
		System.out.println(anzahl[1] + " * Typ B");
		}
		if (anzahl[2] > 0) {
		System.out.println(anzahl[2] + " * Typ C");
		}

		System.out.printf("Der zu zahlende Betrag betr�gt %.2f Euro. \n", (zahlBetrag));
		System.out.println("==========");
		System.out.println("(1) Anzahl Fahrkarten A �ndern.");
		System.out.println("(2) Anzahl Fahrkarten B �ndern.");
		System.out.println("(3) Anzahl Fahrkarten C �ndern.");
		System.out.println("(4) bezahlen.");
		System.out.println("(5) Vorgang abbrechen.");
		System.out.print("Auswahl: ");
		auswahl = tastatur.nextInt();
		System.out.println("\n\n");
		if (auswahl == 1) {
		System.out.print("Wie viele Fahrkarten des Typs A sind gew�nscht?");
		anzahl[0] = tastatur.nextInt();
		}
		if (auswahl == 2) {
		System.out.print("Wie viele Fahrkarten des Typs B sind gew�nscht?");
		anzahl[1] = tastatur.nextInt();
		}
		if (auswahl == 3) {
		System.out.print("Wie viele Fahrkarten des Typs C sind gew�nscht?");
		anzahl[2] = tastatur.nextInt();
		}
		if (auswahl == 5) {
			System.out.println("Sie haben die Option zum Beenden des Vorgangs gew�hlt. Wir w�nschen Ihnen noch einen angenehmen Tag!");
			System.exit(0);
		}
		
		System.out.println("\n\n");
		zahlBetrag = runden((preis[0] * anzahl[0]) + (preis[1] * anzahl[1]) + (preis[2] * anzahl[2]));
		}

		return zahlBetrag;

	}
	
	public static double bezahlen(double zahlBetrag, Scanner tastatur) {
		
		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneM�nze;
		
		while (eingezahlterGesamtbetrag < zahlBetrag) {
			System.out.println("Noch zu zahlen: " + runden(zahlBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingeworfener Betrag (Euro,Cent): ");
			eingeworfeneM�nze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}
		
		return eingezahlterGesamtbetrag;
	}

	public static void ausgabe() {
		
		System.out.println("\nFahrschein wird ausgegeben.");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
		
	}
	
	public static void r�ckgabe(double gesamtBetrag, double zahlBetrag) {
		
		double r�ckgabebetrag = runden(gesamtBetrag - zahlBetrag);
		if (r�ckgabebetrag > 0.0) {
			System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO \n", (r�ckgabebetrag));
			System.out.println("wird in folgenden Scheinen ausgezahlt:");

			while (r�ckgabebetrag >= 20.0) // 20 EURO-Scheine
			{
				System.out.println("20 EURO-Schein");
				r�ckgabebetrag = runden(r�ckgabebetrag - 20.0);
			}
			while (r�ckgabebetrag >= 10.0) // 10 EURO-Scheine
			{
				System.out.println("10 EURO-Schein");
				r�ckgabebetrag = runden(r�ckgabebetrag - 10.0);
			}
			while (r�ckgabebetrag >= 5.0) // 5 EURO-Scheine
			{
				System.out.println("5 EURO-Schein");
				r�ckgabebetrag = runden(r�ckgabebetrag - 5.0);
			}
			System.out.println("");
			System.out.printf("Der R�ckgabe-Restbetrag in H�he von %.2f EURO \n", (r�ckgabebetrag));
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
			{
				System.out.println("2 EURO-M�nze");
				r�ckgabebetrag = runden(r�ckgabebetrag - 2.0);
			}
			while (r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
			{
				System.out.println("1 EURO-M�nze");
				r�ckgabebetrag = runden(r�ckgabebetrag - 1.0);
			}
			while (r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
			{
				System.out.println("50 CENT-M�nze");
				r�ckgabebetrag = runden(r�ckgabebetrag - 0.5);
			}
			while (r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
			{
				System.out.println("20 CENT-M�nze");
				r�ckgabebetrag = runden(r�ckgabebetrag - 0.2);
			}
			while (r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
			{
				System.out.println("10 CENT-M�nze");
				r�ckgabebetrag = runden(r�ckgabebetrag - 0.1);
			}
			while (r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
			{
				System.out.println("5 CENT-M�nze");
				r�ckgabebetrag = runden(r�ckgabebetrag - 0.05);
			}
			while (r�ckgabebetrag >= 0.02)// 2 CENT-M�nzen
			{
				System.out.println("2 CENT-M�nze");
				r�ckgabebetrag = runden(r�ckgabebetrag - 0.02);
			}
			while (r�ckgabebetrag >= 0.01)// 1 CENT-M�nzen
			{
				System.out.println("1 CENT-M�nze");
				r�ckgabebetrag = runden(r�ckgabebetrag - 0.01);
			}
			
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.");
		
	}
}
